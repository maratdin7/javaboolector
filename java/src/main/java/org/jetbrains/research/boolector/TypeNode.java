package org.jetbrains.research.boolector;

public enum TypeNode {
        BoolConst,
        BitvecConst,
        ArrayNode,
        BoolNode,
        BitvecNode,
        Unknown
}
